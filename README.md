# Atedev Attribute Value Converter

This module provide automatic attribute value conversion based on configuration per store.

## Installation
```
composer require atedev/module-attribute-value-converter
bin/magento module:enable Atedev_AttributeValueConverter
bin/magento setup:upgrade
```

## Implementation

1. Declare your converter in di.xml by implementing `Atedev\AttributeValueConverter\Service\ConverterInterface` as VirtualClass
2. Declare your VirtualClass in dedicated Pool `Atedev\AttributeValueConverter\Service\ConverterPoolInterface`, the key used will be your `attribute code`
3. Create your provider in di.xml by extending `Atedev\AttributeValueConverter\Model\Provider`
4. Declare your provider in dedicated Pool `Atedev\AttributeValueConverter\Service\ProviderPoolInterface`, the key used will be your `attribute code`
5. Use the class `Atedev\AttributeValueConverter\Model\Entity\Attribute\Frontend\Value` as frontend model of your attribute
6. Create system.xml and populate with group named with your `attribute code` and implement fields `is_enabled`, `default_value` and `convert_to`

## Example
 
You can see a real implementation here [@TODO]