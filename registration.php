<?php
/**
 * Copyright © Atedev, Inc. All rights reserved.
 */

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(
    ComponentRegistrar::MODULE,
    'Atedev_AttributeValueConverter',
    __DIR__
);