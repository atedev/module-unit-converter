<?php

declare(strict_types=1);

namespace Atedev\UnitConverter\Exception;

use Magento\Framework\Exception\LocalizedException;

class AttributeValueConverterException extends LocalizedException
{

}