<?php

declare(strict_types=1);

namespace Atedev\AttributeValueConverter\Exception\Technical;

use Atedev\AttributeValueConverter\Exception\AttributeValueConverterException;

class MissingConverterException extends AttributeValueConverterException
{

}