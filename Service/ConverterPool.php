<?php

declare(strict_types=1);

namespace Atedev\AttributeValueConverter\Service;

use Atedev\AttributeValueConverter\Exception\Technical\InvalidInstanceException;
use Atedev\AttributeValueConverter\Exception\Technical\MissingConverterException;

class ConverterPool implements ConverterPoolInterface
{
    private array $converters = [];

    public function __construct(array $converters = [])
    {
        foreach($converters as $name => $converter)
        {
            if (!$converter instanceof ConverterInterface)
            {
                throw new InvalidInstanceException(
                    __(
                        '%name Converter class need to implement %interface',
                        [
                            'name' => $name,
                            'interface' => ConverterInterface::class
                        ]
                    )
                );
            }
        }
        $this->converters = $converters;
    }

    public function get(string $attributeCode): ConverterInterface
    {
        if (!isset($this->converters[$attributeCode]))
        {
            throw new MissingConverterException(
                __(
                    '%handler converter not found',
                    ['handler' => $attributeCode]
                )
            );
        }

        return $this->converters[$attributeCode];
    }
}