<?php

declare(strict_types=1);

namespace Atedev\AttributeValueConverter\Service;

use Atedev\AttributeValueConverter\Model\ConfigInterfaceFactory;

class Converter implements ConverterInterface
{
    private ProviderPoolInterface $providerPool;
    private ConfigInterfaceFactory $configFactory;

    public function __construct(
        ProviderPoolInterface $providerPool,
        ConfigInterfaceFactory $configFactory
    ) {
        $this->providerPool = $providerPool;
        $this->configFactory = $configFactory;
    }

    public function process($code, $result)
    {
        $config = $this->configFactory->create([
            'groupName' => $code
        ]);

        if (!$config->isEnabled()) {
            return $result;
        }

        $defaultValue = $config->getDefaultValue();
        $value = $config->getValueToConvert();
        if ($value === $defaultValue) {
            return $result;
        }

        return $this->providerPool->get($code)->convert($value, $result);
    }
}