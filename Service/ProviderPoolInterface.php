<?php

declare(strict_types=1);

namespace Atedev\AttributeValueConverter\Service;

use Atedev\AttributeValueConverter\Model\ProviderInterface;

interface ProviderPoolInterface
{
    public function get(string $attributeCode): ProviderInterface;
}