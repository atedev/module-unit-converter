<?php

declare(strict_types=1);

namespace Atedev\AttributeValueConverter\Service;

interface ConverterPoolInterface
{
    public function get(string $unitName): ConverterInterface;
}