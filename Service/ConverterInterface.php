<?php

declare(strict_types=1);

namespace Atedev\AttributeValueConverter\Service;

interface ConverterInterface
{
    public function process($code, $result);
}