<?php

declare(strict_types=1);

namespace Atedev\AttributeValueConverter\Service;

use Atedev\AttributeValueConverter\Exception\Technical\InvalidInstanceException;
use Atedev\AttributeValueConverter\Exception\Technical\MissingProviderException;
use \Atedev\AttributeValueConverter\Model\ProviderInterface;

class ProviderPool implements ProviderPoolInterface
{
    private array $providers = [];

    public function __construct(array $providers = [])
    {
        foreach($providers as $name => $provider)
        {
            if (!$provider instanceof ProviderInterface)
            {
                throw new InvalidInstanceException(
                    __(
                        '%name Provider class need to implement %interface',
                        [
                            'name' => $name,
                            'interface' => ProviderInterface::class
                        ]
                    )
                );
            }
        }
        $this->providers = $providers;
    }

    public function get(string $attributeCode): ProviderInterface
    {
        if (!isset($this->providers[$attributeCode]))
        {
            throw new MissingProviderException(
                __(
                    '%handler provider not found',
                    ['handler' => $attributeCode]
                )
            );
        }

        return $this->providers[$attributeCode];
    }
}