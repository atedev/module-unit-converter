<?php

declare(strict_types=1);

namespace Atedev\AttributeValueConverter\Model\Entity\Attribute\Frontend;

use Atedev\AttributeValueConverter\Service\ConverterPoolInterface;
use Magento\Eav\Model\Entity\Attribute\Frontend\AbstractFrontend;
use Magento\Eav\Model\Entity\Attribute\Source\BooleanFactory;
use Magento\Framework\DataObject;

class Value extends AbstractFrontend
{
    private ConverterPoolInterface $converterPool;

    public function __construct(
        BooleanFactory $attrBooleanFactory,
        ConverterPoolInterface $converterPool
    ) {
        parent::__construct($attrBooleanFactory);
        $this->converterPool = $converterPool;
    }

    public function getValue(DataObject $object)
    {
        $result = parent::getValue($object);
        $code = $this->getAttribute()->getCode();
        return $this->converterPool->get($code)->process($code, $result);
    }
}
