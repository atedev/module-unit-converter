<?php

declare(strict_types=1);

namespace Atedev\AttributeValueConverter\Model;

interface ConfigInterface
{
    /** @var string */
    public const PATH_SECTION = 'attribute_value_converter';

    /** @var string */
    public const FIELD_IS_ENABLED = 'is_enabled';

    /** @var string */
    public const FIELD_DEFAULT_VALUE = 'default_value';

    /** @var string */
    public const FIELD_CONVERT_TO = 'convert_to';

    public function isEnabled(): bool;

    public function getDefaultValue(): ?string;

    public function getValueToConvert(): ?string;
}