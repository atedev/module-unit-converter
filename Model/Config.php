<?php

declare(strict_types=1);

namespace Atedev\AttributeValueConverter\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;

class Config implements ConfigInterface
{
    private string $groupName;

    public function __construct(
        ScopeConfigInterface $scopeConfig,
        string $groupName
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->groupName = $groupName;
    }

    public function isEnabled(): bool
    {
        return $this->scopeConfig->isSetFlag($this->getPath(self::FIELD_IS_ENABLED));
    }

    public function getDefaultValue(): ?string
    {
        return $this->scopeConfig->getValue($this->getPath(self::FIELD_DEFAULT_VALUE));
    }

    public function getValueToConvert(): ?string
    {
        return $this->scopeConfig->getValue($this->getPath(self::FIELD_CONVERT_TO));
    }

    private function getPath(string $field): string
    {
        return self::PATH_SECTION . '/' . $this->groupName . '/' . $field;
    }
}