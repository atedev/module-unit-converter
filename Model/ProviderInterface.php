<?php

declare(strict_types=1);

namespace Atedev\AttributeValueConverter\Model;

interface ProviderInterface
{
    public function convert($value, $result): mixed;
}