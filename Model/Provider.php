<?php

declare(strict_types=1);

namespace Atedev\AttributeValueConverter\Model;

abstract class Provider implements ProviderInterface
{
    abstract public function convert($value, $result): mixed;
}